# $Id$
# To be included by limit_submit to specify the parameters for
# combined workspaces

wsfiles = 'H_4l_%d.root'   # %d -> mass
wsopt   = " -w combined -d obsData"               # default " -w combWS -m ModelConfig -d combData" (note leading space)

parm = {
# M    mumin mumax  toys/job
# ===  ===== =====  ========
  220: [ 0,   40,   10000 ],
}
