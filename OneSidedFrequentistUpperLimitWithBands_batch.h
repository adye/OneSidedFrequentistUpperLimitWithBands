// $Id$

#ifndef ONESIDEDFREQUENTISTUPPERLIMITWITHBANDS_BATCH_H
#define ONESIDEDFREQUENTISTUPPERLIMITWITHBANDS_BATCH_H

#include <vector>
#ifndef ROOT_Rtypes
#include "Rtypes.h"
#endif

class TH1;
class TTree;
class TCanvas;
class TFile;
class TObjArray;
class RooRealVar;
class RooAbsData;
class RooArgSet;
class RooWorkspace;
class RooDataSet;
namespace RooStats {
  class PointSetInterval;
  class FeldmanCousins;
  class ConfidenceBelt;
  class ModelConfig;
  class ProofConfig;
  class TestStatSampler;
};

class OneSidedFrequentistUpperLimitWithBands_batch {

 public:

  typedef enum { allPoints, singlePoint, signalOnly, bkgOnly } ScanMode;

  OneSidedFrequentistUpperLimitWithBands_batch();
  OneSidedFrequentistUpperLimitWithBands_batch(const char* args);
  OneSidedFrequentistUpperLimitWithBands_batch(const char* infile,
                                               const char* workspaceName,
                                               const char* modelConfigName = "ModelConfig",
                                               const char* dataName = "combData",
                                               const char* sampleFiles = 0,
                                               const char* bkgFiles = 0);
  ~OneSidedFrequentistUpperLimitWithBands_batch();
  void SetupCanvas();

  int OpenWorkspace (const char* infile,
                     const char* workspaceName,
                     const char* modelConfigName,
                     const char* dataName);
  int SetupFeldmanCousins (RooAbsData* parameterScan);
  int SetupLimitCalculator();

  RooStats::PointSetInterval* GetInterval(Bool_t fSaveBeltToFile = kTRUE);

  TObjArray* ReadSamplingDistributions (const char* files, Double_t pointTol = 1e-13);

  RooStats::PointSetInterval* GetInterval(const TObjArray* samplingDistArray,
                                          Bool_t fSaveBeltToFile = kFALSE);

  Double_t GetObservedLimit (const TObjArray* samplingDistArray);
  void GetCLsb (TTree* toyTree);

  void SetupBackgroundSnapshot(const char* snapshotName="paramsToGenerateData", Double_t POIval= 0.0);
  TTree* BkgToys (int nBkgToyMC, const char* snapshotName="paramsToGenerateData", const char* savent = "Background.root");

  TTree* ReadBackgroundNtuples (const char* bkgFiles);

  TTree* GetUpperLimits (TTree* readTree,
                         const char* savent = 0,
                         Double_t pointTol = 1e-13);

  void get_quantiles (std::vector<Double_t>& v, Double_t *qval, Double_t *qerr, int nsamples= 1000);

  int Limit (const char* infile,
             const char* workspaceName = "combWS",
             const char* modelConfigName = "ModelConfig",
             const char* dataName = "combData",
             const char* sampleFiles = 0,
             const char* bkgFiles = 0);
  int Limit (int argc, const char* const* argv);

  // Parameters
  const char* wsfilename;
  ScanMode scanMode;       // allPoints, singlePoint, signalOnly, or bkgOnly
  int      scanPoint;      // for singlePoint: modulo nPointsToScan, gives mu point number.
                           // Also used for random number seed (all modes)
  int      nPointsToScan;  // Number of mu points to scan in [muMin:muMax]
  int      nToys;          // number of signal or background toys
  int      nProofWorkers;  // If PROOF is used
  double   invMass;        // Only required for final result ntuple
  double   muMin;
  double   muMax;
  double   confidenceLevel;
  bool     interpolate;
  std::vector<Double_t> pointsToScan;

  const Double_t*    quantiles;
  const char* const* quantname;
  int  nquantiles;

  // instance variables
  TCanvas* canvas;
  int      ipad;
  int      nToysSig;
  TH1*     histTS;
  TH1*     histUL;

  UInt_t seed;
  TFile* wsfile;
  RooWorkspace* w;
  RooStats::ProofConfig* pc;
  RooStats::FeldmanCousins* fc;
  RooStats::ModelConfig* mc;
  RooRealVar* firstPOI;
  RooAbsData* data;
  double observedUL;
  TObjArray* saved;
  RooStats::ConfidenceBelt* belt;

 private:
  void Init();
};

#endif
