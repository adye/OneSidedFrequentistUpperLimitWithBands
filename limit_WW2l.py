# $Id$
# To be included by limit_submit to specify the parameters for
# gg workspaces

wsfiles = 'H_WW2l_%d.root'   # %d -> mass
##wsopt   = " -w ws -m modelConfig -d data"          # default " -w combWS -m ModelConfig -d combData"

parm = {
# M    mumin mumax  toys/hour
# ===  ===== =====  ========
  120: [ 0,   60,    8000 ],
  130: [ 0,   40,    8000 ],
  140: [ 0,   20,    8000 ],
  150: [ 0,   20,    8000 ],
  160: [ 0,   10,    8000 ],
  165: [ 0,   10,    8000 ],
  170: [ 0,   10,    8000 ],
  180: [ 0,   20,    8000 ],
  190: [ 0,   40,    8000 ],
  200: [ 0,   40,    8000 ],
}
