# $Id$
# To be included by limit_submit to specify the parameters for
# WWlnuqq workspaces

wsfiles     = 'H_WWlnuqq_%d.root'        # %d -> mass
##wsopt       = " -w HWW_Workspace"      # default " -w combWS -m ModelConfig -d combData"
points1     = ".1/.005,1/.05,10/.2,100/1,250/2,/5"
points2     = "10/.2,100/1,250/2,/5"

parm = {
# M    mumin mumax  toys/hour
# ===  ===== =====  ========
  200: [ 0,  500,    5000, points2 ],
  220: [ 0,  500,    5000, points2 ],
  240: [ 0,  500,    5000, points2 ],
  260: [ 0,  500,    5000, points2 ],
  280: [ 0,  500,    5000, points2 ],
  300: [ 0,  500,    5000, points2 ],
  320: [ 0,  500,    5000, points2 ],
  340: [ 0,  500,    5000, points2 ],
  360: [ 0,  500,    5000, points2 ],
  380: [ 0,  500,    5000, points2 ],
  400: [ 0,  500,    5000, points2 ],
  420: [ 0,  300,    5000, points2 ],
  440: [ 0,  300,    5000, points2 ],
  460: [ 0,  300,    5000, points1 ],
  480: [ 0,  300,    5000, points1 ],
  500: [ 0,  300,    5000, points1 ],
  520: [ 0,  300,    5000, points1 ],
  540: [ 0,  300,    5000, points1 ],
  560: [ 0,  300,    5000, points2 ],
  580: [ 0,  500,    5000, points2 ],
  600: [ 0,  500,    5000, points2 ],
}
