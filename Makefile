#===============================================================================
# $Id$
#
# Description:
#    Makefile for a simple stand-alone program using ROOT, RooFit, and RooStats.
#
# Instructions:
#    1. Make sure the ROOTSYS environment variable is set.
#       - The nicest way to do this is: . /ROOT_LOCATION/bin/thisroot.sh
#    2. run: make BINARY
#       - Builds BINARY from BINARY.o
#       - Add ROOTBUILD=debug for debug version.
#       - Add VERBOSE=1 to show main commands as they are executed.
#
# Author: Tim Adye <T.J.Adye@rl.ac.uk>
#===============================================================================

# ROOT setup
ifeq ($(ROOTSYS),)
$(error $$ROOTSYS is not defined)
endif
include $(ROOTSYS)/test/Makefile.arch
ifeq ($(ROOTCONFIG),)
$(error $(ROOTSYS)/test/Makefile.arch did not define $$ROOTCONFIG)
endif

# Variables and functions
PROGRAM       = OneSidedFrequentistUpperLimitWithBands_batch
ROOTOBJECTS   =
OBJECTS       =

OBJECTS      += $(ROOTOBJECTS) $(ROOTOBJECTS:.o=Dict.o)
INSTALLED_LIBS= $(patsubst $(1)/lib%.$(DllSuf),-l%,$(wildcard $(patsubst %,$(1)/lib%.$(DllSuf),$(2))))
_            := @$(if $(filter-out 0,$(VERBOSE)),set -x;,)

ROOTLIBDIR   := $(shell $(ROOTCONFIG) --libdir)
ROOTLIBS     += $(call INSTALLED_LIBS,$(ROOTLIBDIR),HistFactory XMLParser RooStats RooFit RooFitCore Thread Minuit Foam MathMore Html)
TARFILE       = $(PROGRAM).tar.gz

# Configuration
.PHONY      : default standalone clean commands
.SUFFIXES   : .cc .C .$(SrcSuf)
.INTERMEDIATE : $(ROOTOBJECTS:.o=Dict.h)

# Remove default compile-and-link rules
%$(ExeSuf)  : %.cc
%$(ExeSuf)  : %.C
%$(ExeSuf)  : %.$(SrcSuf)
%$(ExeSuf)  : %.o

# Implicit rules
%.$(ObjSuf) : %.cc
	@echo "Compiling $<"
	$(_)$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c -o $@ $<

%.$(ObjSuf) : %.C
	@echo "Compiling $<"
	$(_)$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c -o $@ $<

%.$(ObjSuf) : %.$(SrcSuf)
	@echo "Compiling $<"
	$(_)$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c -o $@ $<

%Dict.$(SrcSuf) %Dict.h : %.h
	@echo "Generating dictionary $@"
	$(_)$(ROOTCINT) -f $@ -c -p $(CXXFLAGS) $(CPPFLAGS) $< $(wildcard $(patsubst %.h,%LinkDef.h,$<))

%$(ExeSuf)  : %.o $(OBJECTS)
	@echo "Making executable $@"
	$(_)$(LD) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) $(ROOTLIBS) $(OutPutOpt)$@
	$(_)$(MT_EXE)

# Targets
ifeq ($(PROGRAM),BINARY)
default     : help
else
default     : $(PROGRAM)
standalone  : $(TARFILE)
clean       :
	rm -f $(ROOTOBJECTS:.o=Dict.$(SrcSuf)) $(ROOTOBJECTS:.o=Dict.h) $(OBJECTS) $(PROGRAM).$(ObjSuf) $(PROGRAM)$(ExeSuf) $(TARFILE)
endif

help        :
	@echo "Usage: $(MAKE) $(PROGRAM) [ROOTBUILD=debug] [VERBOSE=1]"

# Make tar file with program and all required shared libraries
# for submission to the Grid.
$(TARFILE)  : $(PROGRAM) $(STANDALONE_FILES)
	@echo "Making $@"
	@rm -r -f tartree
	$(_)mkdir -p tartree/root/lib
	$(_)ln -s $(ROOTSYS)/etc $(ROOTSYS)/fonts tartree/root
	$(_)ln -nfs $(ROOTSYS)/lib/libHistPainter.so $(ROOTSYS)/lib/libTreePlayer.so \
                    `ldd $(PROGRAM) | sed -n 's~^.* => \([^ ]*/[^/ ]*\).*~\1~p' | grep -v '^/lib\(64\)\?/'` tartree/root/lib
	$(_)tar zchf $@ $^ -C tartree root
	@rm -r -f tartree

commands    :
	@echo "Compile: $(CXX) $(CXXFLAGS) $(CPPFLAGS) -c -o $(PROGRAM).$(ObjSuf) $(PROGRAM).$(SrcSuf)"
	@echo "Link:    $(LD) $(LDFLAGS) $(PROGRAM).$(ObjSuf) $(OBJECTS) $(LOADLIBES) $(LDLIBS) $(ROOTLIBS) $(OutPutOpt)$(PROGRAM)$(ExeSuf)"
