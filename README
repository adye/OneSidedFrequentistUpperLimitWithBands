PhysicsAnalysis/HiggsPhys/CombinationTools/RooStatTools/trunk/OneSidedFrequentistUpperLimitWithBands
// $Id$

This package performs the Higgs cross-section upper limit calculation
using multiple jobs on a local batch system or the Grid.

The actual calculation is performed with
OneSidedFrequentistUpperLimitWithBands_batch.C, which can run
signal+background and/or background-only toys. For batch/Grid running,
the results for a few toys (in this case, signal or background, not both)
in each job are writen to SamplingDistributions.root or Background.root
files. These can then be loaded back into OneSidedFrequentistUpperLimitWithBands_batch
to calculate the final limits.

See the comments at the top of OneSidedFrequentistUpperLimitWithBands_batch.C
for a summary of the calculation. A brief summary of the program options can be
obtained by running OneSidedFrequentistUpperLimitWithBands_batch -h .

The code requires ROOT 5.28a or later. Some of the Higgs combination workspaces
(H_comb_AaronArmbruster/trunk/without_dummy) also require a fix in 5.29.02
(or use modified ToyMCSampler.cxx in that directory).
The Grid version will send the currently-set ROOT shared libraries with the job.

Grid setup:

  voms-proxy-init -voms atlas
  . /afs/cern.ch/atlas/offline/external/GRID/DA/panda-client/latest/etc/panda/panda_setup.sh

Submission:

Check the parameters specified near the top of limit_submit, especially the
workspace files and output dataset name. For the Grid, the jobs will be submitted
with Panda's prun command. The prun command might have to be modified,
eg. to exclude bad sites (prun --excludedSite=SITE) or request automatic transfer
of the output (prun --destSE=SE).

Then, to submit the 160 GeV fit:

  ./limit_submit 160

Use -t to test and see what it'll do before filling up the world-wide Grid.
Each prun command will submit one job-set, which will run several jobs (eg. 100).
Use -b to submit to batch (edit your batch submission command in limit_submit,
currently set up for RAL PPD), where there is no grouping into job-sets.

Specify -w gg, -w WW2l, or -w WWlnuqq to use a different set of
workspaces (parameters defined in limit_*.py files) instead of
the default combined workspaces (limit_comb.py).
Use ./limit_submit -h for help with other options.

The command in the --exec option (as printed with limit_submit -t),
is just ./limit_job_grid (same as OneSidedFrequentistUpperLimitWithBands_batch,
but with some Grid setup) with options.
The options are (from OneSidedFrequentistUpperLimitWithBands_batch help):

  ./OneSidedFrequentistUpperLimitWithBands_batch WORKSPACE-FILE.root \
      -w workspace -m modelConfig -d dataset \
      -S (S+B only) -B (background-only) -p scanPoint -n nPointsToScan \
      -t nToys -M invMass -1 muMax

limit_submit sets The -p scanPoint option to %RNDM:0, which is replaced by
Panda with 0,1,2...nJobs for each job and ensures there are different
random number seeds for each job.


Retrieving the results:

Once submitted, check your Panda page, linked from here:

  http://panda.cern.ch/server/pandamon/query?ui=users

You'll see one dataset per job set, which you need to get back to
local storage. It's quite small, 2-5 MB per dataset.

If a destSE was specified on the prun command, then the output should be
automatically transferred to the specified storage.
Alternatively, you can request this transfer using the DDM web interface.
See here: http://panda.cern.ch/server/pandamon/query?mode=ddm_req

A third option (which I found to be much the fastest) is to retrieve it
with dq2-get, eg.

  dq2-get -f "*.root" "user.adye.H_comb_*_AaronArmbruster.8.*/"

to get all the datasets back (note the quotes to prevent the
wildcards being expanded by the shell). If you want the logfiles too,
then leave out the -f "*.root" - or look at them on Panda web.

The files are combined using wildcards, so make sure that all the files for
one mass can be distinguished by wildcard, eg.
user.adye.H_comb_160_AaronArmbruster.8.Smp*/*.SamplingDistributions.root
and
user.adye.H_comb_160_AaronArmbruster.8.Bkg*/*.Background.root
Files can be accessed from another system with the TUrl syntax
(eg. root://server/file.root) and the full path can contain wildcards.

Combining the results:

For each mass point, run OneSidedFrequentistUpperLimitWithBands_batch,
specifying the SamplingDistributions and Background files by wildcard, eg.

  OneSidedFrequentistUpperLimitWithBands_batch H_comb_160_AaronArmbruster.root \
    -M 160 \
    -s "user.adye.H_comb_160_AaronArmbruster.8.Smp*/*.SamplingDistributions.root" \
    -b "user.adye.H_comb_160_AaronArmbruster.8.Bkg*/*.Background.root"

Note the use of quotes to prevent the wildcards being expanded by shell.
The -M option is only needed so the final results are plotted in the right place.
OneSidedFrequentistUpperLimitWithBands_batch will then calculate and print the
limits for this point. It will also write a file H_comb_160_AaronArmbruster-Results.root
with the results.

This step takes a few minutes each, so if you are impatient and have a
multi-CPU machine you might consider spawning subprocesses with "&",
or submitted them as quick local batch jobs.

Once all the mass points have been done, a plot and table of results can be
made using:

  root limitPlot.C

This will use all files *-Results.root. Different file names and limits can be
set using the options or editing limitPlot.C.



AUTHORS

OneSidedFrequentistUpperLimitWithBands_batch.C is based on
OneSidedFrequentistUpperLimitWithBands_proof.C of 3 March 2011 from Aaron Armbruster.
It was, in turn, based on OneSidedFrequentistUpperLimitWithBands.C of 28 January 2011
from Kyle Cranmer, with contributions from Haichen Wang and Daniel Whiteson.

Modifications and scripts for running in batch or on the Grid are by Tim Adye, 21 March 2011.
