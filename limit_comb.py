# $Id$
# To be included by limit_submit to specify the parameters for
# combined workspaces

wsfiles     = 'H_comb_%d_AaronArmbruster.root'   # %d -> mass
mustep = '1/0.1,2/0.2,10/0.5,/1'
mustep2= '0.2/0.02,1/0.1,2/0.2,10/0.5,/1'

parm = {
# M    mumin mumax  toys/hour
# ===  ===== =====  ========
  110: [ 0,   12,    4000 ],
  115: [ 0,   10,     150 ],
  120: [ 0,    8,      70 ],
  130: [ 0,    6,      70 ],
  140: [ 0,    4,     120 ],
  150: [ 0,    3,     200 ],
  160: [ 0,    3,     200 ],
  170: [ 0,    3,     200 ],
  180: [ 0,    3,     200 ],
  190: [ 0,    4,     200 ],
  200: [ 0,    8,      60 ],
  220: [ 0,    8,      60 ],
  240: [ 0,    8,      60 ],
  260: [ 0,    8,      60 ],
  280: [ 0,    8,      60 ],
  300: [ 0,    8,     100 ],
  320: [ 0,   20,    1000 ],
  340: [ 0,   18,    1000 ],
  360: [ 0,   14,    1000 ],
  380: [ 0,   14,    1000 ],
  400: [ 0,   14,    1000 ],
  420: [ 0,   40,    1000 ],
  440: [ 0,   40,    1000 ],
  460: [ 0,   40,    1000 ],
  480: [ 0,   40,    1000 ],
  500: [ 0,   40,    1000 ],
  520: [ 0,   40,    1000 ],
  540: [ 0,   40,    1000, mustep2 ],
  560: [ 0,   40,    1000, mustep2 ],
  580: [ 0,   40,    1000 ],
  600: [ 0,   40,    1000 ],
}
