# $Id$
# To be included by limit_submit to specify the parameters for
# gg workspaces

wsfiles = 'H_gg_%d.root'   # %d -> mass
##wsopt   = " -w WS -d dataset"          # default " -w combWS -m ModelConfig -d combData"
mustep = "10/0.5,/1"

parm = {
# M    mumin mumax  toys/hour
# ===  ===== =====  ========
  110: [ 0,   60,   10000 ],
  115: [ 0,   60,   10000 ],
  120: [ 0,   60,   10000 ],
  130: [ 0,   60,   10000 ],
  140: [ 0,   60,   10000 ],
}
