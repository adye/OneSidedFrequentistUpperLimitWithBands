// $Id$
// Plot Higgs cross-section upper limits a different masses.
// Uses results from OneSidedFrequentistUpperLimitWithBands_batch.C
#include <iostream>
#include <iomanip>

#include "TString.h"
#include "TRegexp.h"
#include "TFile.h"
#include "TChain.h"
#include "TChainElement.h"
#include "TObject.h"
#include "TList.h"
#include "TVectorD.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TH2F.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TLatex.h"
#include "TLine.h"
#include "TLegend.h"

using std::cout;
using std::endl;
using std::setw;

TCanvas* c1=0;

void printLimit (double lim, double err=0, double muMin=0, double muMax=1e30, bool showerr=true)
{
  cout << std::fixed << setprecision(2) << setw(7) << lim;
  if (showerr && err>0.0) cout << "+/-" << setw(4) << err;
  cout << (lim<=muMin||lim>=muMax?'*':' ');
  if (showerr && err<=0.0) cout << setw(7) << " ";
}

void limitPlot (int         logmu= 1,   // 0=linear, 1=log plots, 2=suppress error printout
                const char* files= "*-Results*.root",
                double      mlo=   118.0,
                double      mhi=   192.0,
                double      mulo=  0,
                double      muhi= -1)
{
  bool showerr= logmu!=2;
  if (muhi<=mulo) {
    if (logmu) {
      mulo=  0.1;
      muhi=  1e3;
    } else {
      mulo=  0;
      muhi=  40.5;
    }
  }

  cout << "Read limit files " << files << endl;
  TChain bandTree ("band", "OneSidedFrequentistUpperLimitWithBands");
  bandTree.Add (files);
  Int_t nm = bandTree.GetEntries();
  TIter next(bandTree.GetListOfFiles());
  const TChainElement* ntfile;
  TString nam;
  int namok=0;
  while ((ntfile= dynamic_cast<const TChainElement*>(next()))) {
    cout << "Limit file " << ntfile->GetTitle() << " has " << ntfile->GetEntries() << " entries" << endl;
    TString fl(ntfile->GetTitle());
    Ssiz_t l;
    Ssiz_t i= fl.Index(TRegexp("H_[^_*?]*_[1-6][0-9][05][_.-]"),&l);
    if (i!=kNPOS) {
      TString namt = fl(i+2,l-7);
      if (namok==0) {
        nam= namt;
        namok= 1;
      } else if (namok==1 && namt!=nam) {
        nam= "";
        namok=2;
      }
    }
  }
  if (namok==2) namok= 0;
  if (namok) cout << "Samples " << nam << endl;

  double invMass=0;
  double band2sigDown=0, band1sigDown=0, bandMedian=0, band1sigUp=0, band2sigUp=0;
  double band2sigDown_err=0, band1sigDown_err=0, bandMedian_err=0, band1sigUp_err=0, band2sigUp_err=0;
  double obsUL=0, CLb=0, muMin=0, muMax=0, confidenceLevel=0;
  Int_t nPointsToScan=0, nToysSig=0, nToysBkg=0;
  UInt_t seed=0;
  bandTree.SetBranchAddress ("invMass",      &invMass);
  bandTree.SetBranchAddress ("band2sigDown", &band2sigDown);
  bandTree.SetBranchAddress ("band1sigDown", &band1sigDown);
  bandTree.SetBranchAddress ("bandMedian",   &bandMedian);
  bandTree.SetBranchAddress ("band1sigUp",   &band1sigUp);
  bandTree.SetBranchAddress ("band2sigUp",   &band2sigUp);
  bandTree.SetBranchAddress ("obsUL",        &obsUL);
  bandTree.SetBranchAddress ("CLb",          &CLb);
  bandTree.SetBranchAddress ("seed",         &seed);
  bandTree.SetBranchAddress ("muMin",        &muMin);
  bandTree.SetBranchAddress ("muMax",        &muMax);
  bandTree.SetBranchAddress ("confidenceLevel",&confidenceLevel);
  bandTree.SetBranchAddress ("nPointsToScan",&nPointsToScan);
  bandTree.SetBranchAddress ("nToysSig",     &nToysSig);
  bandTree.SetBranchAddress ("nToysBkg",     &nToysBkg);
  if (bandTree.GetBranch("bandMedian_err")) {
    bandTree.SetBranchAddress ("band2sigDown_err", &band2sigDown_err);
    bandTree.SetBranchAddress ("band1sigDown_err", &band1sigDown_err);
    bandTree.SetBranchAddress ("bandMedian_err",   &bandMedian_err);
    bandTree.SetBranchAddress ("band1sigUp_err",   &band1sigUp_err);
    bandTree.SetBranchAddress ("band2sigUp_err",   &band2sigUp_err);
  }

  int w= 8+(showerr?7:0);
  cout << setw(9) << "M (GeV)"
       << setw(w) << "-2sigma"
       << setw(w) << "-1sigma"
       << setw(w) << "expected"
       << setw(w) << "+1sigma"
       << setw(w) << "+2sigma"
       << setw(9) << "observed"
       << setw(9) << "CLb"
       << setw(9) << "mu Step"
       << setw(9) << "mu Max"
       << setw(9) << "#toy S+B"
       << setw(9) << "#toy B"
       << endl;
  TVectorD x(nm), c(nm), obs(nm), ebp1(nm), ebm1(nm), ebp2(nm), ebm2(nm), zero(nm);
  Double_t* lim[]= {&band1sigDown, &bandMedian, &band1sigUp, &band2sigUp, &obsUL};
  for (int i=0;i<nm;i++){
    bandTree.GetEntry(i);
    Double_t step= (nPointsToScan>0 ? (muMax-muMin)/nPointsToScan : 0);
    cout << std::fixed << setprecision(0)
         << setw(8) << invMass << ' ';
    printLimit (band2sigDown, band2sigDown_err, muMin, muMax, showerr);
    printLimit (band1sigDown, band1sigDown_err, muMin, muMax, showerr);
    printLimit (bandMedian,   bandMedian_err,   muMin, muMax, showerr);
    printLimit (band1sigUp,   band1sigUp_err,   muMin, muMax, showerr);
    printLimit (band2sigUp,   band2sigUp_err,   muMin, muMax, showerr);
    cout << std::fixed << setprecision(3)
         << setw(8) << obsUL <<        (obsUL       <=muMin||obsUL       >=muMax?'*':' ')
         << std::fixed << setprecision(2)
         << setw(8) << CLb << ' '
         << std::fixed << setprecision(1)
         << setw(8) << step << ' '
         << std::fixed << setprecision(0)
         << setw(8) << muMax << ' '
         << setw(8) << nToysSig << ' '
         << setw(8) << nToysBkg
         << endl;
    for (size_t j=0; j<sizeof(lim)/sizeof(lim[0]); j++) {
      if (*lim[j]<=muMin) { *lim[j]= mulo; }
      if (*lim[j]>=muMax) { *lim[j]= muhi; }
    }
    x[i]= invMass;
    obs[i]= obsUL;
    c[i]= bandMedian;
    ebp1[i]=band1sigUp-bandMedian;
    ebm1[i]=bandMedian-band1sigDown;
    ebp2[i]=band2sigUp-bandMedian;
    ebm2[i]=bandMedian-band1sigDown;  // -1sigma from PCL
    zero[i]= 0.0;
  }

  TGraph* go= new TGraph (x, obs);
  go->SetLineColor(1);
  go->SetLineWidth(2);
  go->SetMarkerStyle(20);
  go->SetMarkerColor(1);

  TGraph* gc= new TGraph (x, c);
  gc->SetLineColor(1);
  gc->SetLineStyle(2);
  gc->SetLineWidth(2);

  TGraph* gb1= new TGraphAsymmErrors (x, c, zero, zero, ebm1, ebp1);
  gb1->SetFillColor(3);

  TGraph* gb2= new TGraphAsymmErrors (x, c, zero, zero, ebm2, ebp2);
  gb2->SetFillColor(5);

  TGraph* glim= 0;

  if (!c1) c1= new TCanvas("limitPlot","limitPlot",1050,750);
  c1->SetLogy(logmu?1:0);
  gPad->SetTicks(0,1);
  gStyle->SetOptStat(0);

  TH2F* clplot= new TH2F ("limits", nam, 50, mlo, mhi, 50, mulo, muhi);

  clplot->Draw();
  clplot->SetXTitle("m_{H} [GeV]");
  clplot->SetYTitle("95% C.L. upper bound on #sigma/#sigma_{SM}");

  gb2->Draw("e3");
  gb1->Draw("e3");
  gc->Draw("l");
  go->Draw("lp");
  if (glim) glim->Draw("p");

  TLegend* leg = new TLegend (0.7, 0.7, 0.89, 0.89);
  leg->AddEntry(go,"Observed","lp");
  leg->AddEntry(gc,"Expected","l");
  leg->AddEntry(gb1,"#pm 1#sigma","f");
  leg->AddEntry(gb2,"+ 2#sigma","f");
  if (glim) leg->AddEntry(glim,"scan limit","p");
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->Draw("same");

  if (logmu || (mulo<1 && muhi<100)) {
    TLine* line = new TLine (mlo, 1.0, mhi, 1.0);
    line->SetLineColor(2);
    line->SetLineStyle(3);
    line->Draw();
  }

  TString psnam;
  psnam.Form("H_%s%s.ps",(nam.Length()==0 ? "limit" : nam.Data()),(logmu?"":"_lin"));
  c1->Print (psnam);
}
