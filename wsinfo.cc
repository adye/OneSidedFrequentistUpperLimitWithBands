#include <iostream>
#include <list>
#include "TROOT.h"
#include "TList.h"
#include "TFile.h"
#include "TKey.h"
#include "TClass.h"
#include "TIterator.h"

#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"

using std::cout;
using std::cerr;
using std::endl;

void wsinfo (Int_t verbose= 0)
{
  // Show workspace summary
  // Example: root -l -q -b *.root wsinfo.cc+

  TIter iter(gROOT->GetListOfFiles());
  while (TFile* f = dynamic_cast<TFile*>(iter())) {
    int nws= 0;
    TIter nextkey (f->GetListOfKeys());
    while (TKey* key= dynamic_cast<TKey*>(nextkey())) {
      const TClass* c= TClass::GetClass (key->GetClassName());
      if (c && c->InheritsFrom (RooWorkspace::Class())) {
        if (const RooWorkspace* ws= dynamic_cast<const RooWorkspace*>(key->ReadObj())) {
          nws++;
          if (verbose) ws->Print();
          cout << f->GetName() << " -w " << ws->GetName();
          std::list<TObject*> objs= ws->allGenericObjects();
          for (std::list<TObject*>::const_iterator mcp= objs.begin(); mcp!=objs.end(); mcp++) {
            if (const RooStats::ModelConfig* mc= dynamic_cast<const RooStats::ModelConfig*>(*mcp)) {
              cout << " -m " << mc->GetName() << " (";
              TIterator *iterator= mc->GetParametersOfInterest()->createIterator();
              while (const RooAbsArg* nextparm= dynamic_cast<const RooAbsArg*>(iterator->Next()))
                cout << nextparm->GetName() << ",";
              cout << mc->GetObservables()->getSize() << " obs,"
                   << mc->GetGlobalObservables()->getSize() << " glob";
              if (mc->GetNuisanceParameters())
                cout << "," << mc->GetNuisanceParameters()->getSize() << " nuisance";
              cout << (mc->GetPdf()->canBeExtended() ? ",ext" : ",noext") << ")";
            }
          }
          std::list<RooAbsData*> datas= ws->allData();
          for (std::list<RooAbsData*>::const_iterator datap= datas.begin(); datap!=datas.end(); datap++) {
            const RooAbsData* data= *datap;
            if (data) cout << " -d " << data->GetName() << " (" << data->numEntries() << ")";
          }
          cout << endl;
          delete ws;
        }
      }
    }
    if      (nws==0) cerr << f->GetName() << " has no workspace" << endl;
    else if (nws>=2) cerr << f->GetName() << " has "<<nws<<" workspaces" << endl;
  }
}
